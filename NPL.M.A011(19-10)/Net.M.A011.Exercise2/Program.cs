﻿class Program
{
    static void Main(string[] args)
    {
        int[] numbers = { 1, 2, 3, 3, 5, 6, 5, 2 };
        int[] distinctNumbers = numbers.RemoveDuplicate();
        Console.WriteLine(string.Join(", ", distinctNumbers));

        string[] names = { "Hung", "Vu", "Van", "Hung", "Quang", "Huy", "Vu" };
        string[] distinctNames = names.RemoveDuplicate();
        Console.WriteLine(string.Join(", ", distinctNames));
    }
}