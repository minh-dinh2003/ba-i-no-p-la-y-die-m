﻿using Net.M.A011.Exercise3;

class Program
{
    static void Main(string[] args)
    {
        int[] numbers = { 1, 2, 3, 5, 7, 3, 2 };
        int index1 = numbers.LastIndexOf(3);
        Console.WriteLine(index1); 

        string[] names = { "Hoang", "Trong", "A", "Hoang", "Trong", "Hieu" };
        int index2 = names.LastIndexOf("Trong");
        Console.WriteLine(index2);
    }
}