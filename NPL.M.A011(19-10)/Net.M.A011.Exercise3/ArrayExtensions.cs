﻿namespace Net.M.A011.Exercise3
{
    internal static class ArrayExtensions
    {
        public static int LastIndexOf<T>(this T[] array, T elementValue)
        {
            for (int i = array.Length - 1; i >= 0; i--)
            {
                if (EqualityComparer<T>.Default.Equals(array[i], elementValue))
                {
                    return i;
                }
            }

            return -1;
        }
    }
}
