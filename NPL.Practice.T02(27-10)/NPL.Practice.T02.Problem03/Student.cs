﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem03
{
    public enum GraduateLevel
    {
        Excellent,
        VeryGood,
        Good,
        AverageGood,
        Failed
    }

    public class Student : IGraduate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public decimal SqlMark { get; set; }
        public decimal CsharpMark { get; set; }
        public decimal DsaMark { get; set; }
        public decimal GPA { get; set; }
        public GraduateLevel GraduateLevel { get; set; }
        public void Graduate()
        {
            decimal avgMark = (SqlMark + CsharpMark + DsaMark) / 3;
            GPA = Math.Round(avgMark, 2);

            if (avgMark >= 9)
            {
                GraduateLevel = GraduateLevel.Excellent;
            }
            else if (avgMark >= 8 && avgMark < 9)
            {
                GraduateLevel = GraduateLevel.VeryGood;
            }
            else if (avgMark >= 7 && avgMark < 8)
            {
                GraduateLevel = GraduateLevel.Good;
            }
            else if (avgMark >= 5 && avgMark < 7)
            {
                GraduateLevel = GraduateLevel.AverageGood;
            }
            else
            {
                GraduateLevel = GraduateLevel.Failed;
            }
        }
        public string GetCertificate()
        {
            return string.Format("Name: {0,-18}| SqlMark: {1,-5}| CsharpMark: {2,-5}| DsaMark: {3,-5}| GPA: {4,-5}| GraduateLevel: {5}", Name, SqlMark, CsharpMark, DsaMark, GPA, GraduateLevel);
        }
    }
}
