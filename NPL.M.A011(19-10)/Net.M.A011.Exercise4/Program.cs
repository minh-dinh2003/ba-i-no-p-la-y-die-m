﻿int[] numbers = new int[] { 3, 2, 5, 6, 1, 7, 7, 5, 2 };


Console.WriteLine("ElementOfOrder2(): " + numbers.ElementOfOrder2());
Console.WriteLine("ElementOfOrder(2): " + numbers.ElementOfOrder(2));
Console.WriteLine("ElementOfOrder(3): "+ numbers.ElementOfOrder(3));
Console.WriteLine("ElementOfOrder(20): "); 
try
{
    int invalidOrderLargest = numbers.ElementOfOrder(20);
}catch(Exception e)
{
    Console.WriteLine(e.Message);
}// Throws exception