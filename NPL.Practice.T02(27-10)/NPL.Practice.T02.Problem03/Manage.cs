﻿using System.Globalization;

namespace NPL.Practice.T02.Problem03
{
    internal class Manage
    {
        public static List<Student> Students = new List<Student>();
        public static void CreateStudent()
        {
            Student student = new Student();
            int studentId = EnterNumber<int>("Enter student ID:");
            bool checkVar;
            if (Students.Count() > 0)
            {
                do
                {
                    checkVar = true;
                    foreach (Student stu in Students)
                    {
                        if (stu.Id == studentId)
                        {
                            checkVar = false;
                            studentId = EnterNumber<int>("Student ID Existed. Please Input again: ");
                            break;
                        }
                    }
                } while (!checkVar);
            }
            student.Id = studentId;
            Console.WriteLine("Enter student name:");
            student.Name = Console.ReadLine();

            string[] dateFormats = { "yyyy/MM/dd", "yyyy-MM-dd", "dd/MM/yyyy", "dd--MM-yyyy" };
            Console.WriteLine("Enter start date (yyyy/MM/dd):");
            string startDateInput = Console.ReadLine();
            DateTime startDate;

            while (!DateTime.TryParseExact(startDateInput, dateFormats, CultureInfo.InvariantCulture, DateTimeStyles.None, out startDate))
            {
                Console.WriteLine("Invalid date format. Please enter a valid start date (yyyy/MM/dd, yyyy-MM-dd, dd/MM/yyyy, dd--MM-yyyy):");
                startDateInput = Console.ReadLine();
            }
            do
            {
                student.SqlMark = EnterNumber<decimal>("Enter SQL mark: ");
            } while (!(student.SqlMark >= 0 && student.SqlMark <= 10));
            do
            {
                student.CsharpMark = EnterNumber<decimal>("Enter C# mark: ");
            } while (!(student.CsharpMark >= 0 && student.CsharpMark <= 10));
            do
            {
                student.DsaMark = EnterNumber<decimal>("Enter DSA mark: ");
            } while (!(student.DsaMark >= 0 && student.DsaMark <= 10));
            student.Graduate();

            Students.Add(student);
        }

        static T EnterNumber<T>(string Message)
        {
            T output;
            do
            {
                try
                {
                    Console.Write(Message);
                    output = (T)Convert.ChangeType(Console.ReadLine(), typeof(T));
                    return output;
                }
                catch (Exception e)
                {
                    continue;
                }
            } while (true);

        }
    }
}
