﻿using System;
using System.Linq;

public static class ArrayExtensions
{
    public static T ElementOfOrder2<T>(this T[] array) where T : IComparable<T>
    {
        if (array == null || array.Length < 2)
        {
            throw new ArgumentException("The array must contain at least 2 elements.");
        }

        return array.OrderByDescending(x => x).Skip(1).FirstOrDefault();
    }

    public static T ElementOfOrder<T>(this T[] array, int orderLargest) where T : IComparable<T>
    {
        if (orderLargest < 1 || orderLargest > array.Length)
        {
            throw new ArgumentOutOfRangeException(nameof(orderLargest), "The orderLargest value is out of range.");
        }

        return array.OrderByDescending(x => x).Skip(orderLargest - 1).FirstOrDefault();
    }
}