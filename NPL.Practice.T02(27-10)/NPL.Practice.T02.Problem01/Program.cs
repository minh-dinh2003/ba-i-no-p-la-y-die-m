﻿using System;
using System.Text;

namespace NPL.Practice.T02.Problem01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the article content (Press Ctrl + Enter to finish):");
            string content = ReadMultilineInput();

            Console.Write("\nEnter the maximum length of the summary:");
            int maxLength;
            do
            {
                try
                {
                    maxLength = int.Parse(Console.ReadLine());
                    break;
                }catch(FormatException)
                {
                    Console.WriteLine("Please enter Int type!");
                    continue;
                }
            } while (true);
            

            string summary = GetArticleSummary(content, maxLength);
            Console.WriteLine(summary);
        }

        static public string GetArticleSummary(string content, int maxLength)
        {
            if (content.Length <= maxLength)
            {
                return content; 
            }
            else
            {
                string summary = content.Substring(0, maxLength);
                int lastSpaceIndex = summary.LastIndexOf(" ");

                if (lastSpaceIndex != -1)
                {
                    summary = summary.Substring(0, lastSpaceIndex);
                }

                return summary + "...";
            }
        }
        static string ReadMultilineInput()
        {
            List<string> lines = new List<string>();
            StringBuilder currentLine = new StringBuilder();
            bool exitLoop = false;
            ConsoleKeyInfo keyInfo;

            do
            {
                keyInfo = Console.ReadKey(intercept: true);

                if (keyInfo.Key == ConsoleKey.Enter)
                {
                    if (keyInfo.Modifiers == ConsoleModifiers.Control)
                    {
                        exitLoop = true;
                    }
                    else
                    {
                        lines.Add(currentLine.ToString());
                        Console.WriteLine();
                        currentLine.Clear();
                    }
                }
                else if (keyInfo.Key == ConsoleKey.Backspace)
                {
                    if (currentLine.Length > 0)
                    {
                        currentLine.Remove(currentLine.Length - 1, 1);
                        Console.Write("\b \b");
                    }
                }
                else
                {
                    currentLine.Append(keyInfo.KeyChar);
                    Console.Write(keyInfo.KeyChar);
                }
            } while (!exitLoop);

            lines.Add(currentLine.ToString());

            return string.Join(Environment.NewLine, lines);
        }
    }
}