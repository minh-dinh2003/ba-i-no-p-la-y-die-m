﻿using System;
using System.Text.RegularExpressions;

namespace NPL.Practice.T02.Problem02
{
    class Program
    {
        static void Main(string[] args)
        {

            int arrLen = EnterInt("Enter Number of Element in Array: ");
            int[] inputArray = new int[arrLen];
            for(int i =0; i < arrLen; i++)
            {
                inputArray[i] = EnterInt("Enter value of element "+(i+1)+": ");
            }
            int subLength = EnterInt("Enter subarray length: ");

            int maxSum = FindMaxSubArray(inputArray, subLength);
            Console.WriteLine("Maximum contiguous subarray sum: "+maxSum);
        }
        static int EnterInt(string Message)
        {
            int output;
            do
            {
                try
                {
                    Console.Write(Message);
                    output = int.Parse(Console.ReadLine());
                    return output;
                }catch (Exception e)
                {
                    continue;
                }
            }while (true);
        }
        static public int FindMaxSubArray(int[] inputArray, int subLength)
        {
            int maxSum = int.MinValue;

            for (int i = 0; i <= inputArray.Length - subLength; i++)
            {
                int currentSum = 0;

                for (int j = i; j < i + subLength; j++)
                {
                    currentSum += inputArray[j]; 
                }

                if (currentSum > maxSum)
                {
                    maxSum = currentSum; 
                }
            }

            return maxSum;
        }
    }
}