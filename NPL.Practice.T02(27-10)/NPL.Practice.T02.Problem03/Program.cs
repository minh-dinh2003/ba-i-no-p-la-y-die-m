﻿using System;

namespace NPL.Practice.T02.Problem03
{
    class Program
    {
        static void Main(string[] args)
        {
            string choice,YN;
            bool continueFlag = true;
            do
            {
                Console.WriteLine("=========Student Grade==========");
                Console.WriteLine("1. Input Student");
                Console.WriteLine("2. Show Student");
                Console.WriteLine("3. Exit");
                choice = Console.ReadLine();
                switch (choice)
                {
                    case "1":
                        do
                        {
                            Manage.CreateStudent();
                            Console.WriteLine("Do you want to continues?(Yes/No): "); YN = Console.ReadLine();
                        } while (YN.ToLower() == "yes");
                        break;
                    case "2":
                        if (Manage.Students.Count > 0)
                        {
                            foreach (Student student in Manage.Students)
                            {
                                Console.WriteLine(student.GetCertificate());
                            }
                        }
                        else
                        {
                            Console.WriteLine("No Student inputed!");
                        }
                        break;
                    case "3":
                        return;
                }
            } while (true);

            
        }
    }
}